#! /bin/bash

/usr/sbin/apache2ctl -D FOREGROUND -k start &

# shibd doesn't write its pid to a file when not daemonized
/usr/sbin/shibd -f -F -c /etc/shibboleth/shibboleth2.xml &

sleep 2

wait -n $(pidof shibd) $(cat /var/run/apache2/apache2.pid)

exit $?
