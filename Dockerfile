FROM debian:bullseye-slim
LABEL maintainer="swl@stanford.edu"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qq update \
    && apt-get -qq -y --no-install-recommends install \
        curl \
        vim-tiny \
        ca-certificates \ 
        apache2 \
        libapache2-mod-shib \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && mkdir /var/tmp/metadata \
    && chown -R 101:101 /var/tmp/metadata

RUN a2enmod ssl headers shib

VOLUME /etc/shibboleth
VOLUME /etc/apache2/sites-available
VOLUME /etc/apache2/sites-enabled
VOLUME /var/log

COPY start.sh /start.sh
EXPOSE 80 443
CMD ["/start.sh"]


